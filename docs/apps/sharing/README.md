# Sharing

File sharing through Onion Services:

* [OnionShare](https://onionshare.org/): allows you to quickly host an onion
  site, chat, and create a private dropdown in addition to sharing files!
* [sn0b4ll/aria2-onion-downloader](https://github.com/sn0b4ll/aria2-onion-downloader):
  download from .onion-domains faster.
