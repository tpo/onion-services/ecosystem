# Onionsite Checklist

## Introduction

This is a quick and not yet complete checklist intended to help [Onion
Service][] operators to setup and maintain their sites, and goes beyond the
[setup guide][] and the [advanced settings][].

!!! note "Under writing"

    This is still being written, and progress is tracked at
    [tpo/onion-services/ecosystem#19][].

[Onion Service]: https://community.torproject.org/onion-services/
[setup guide]: https://community.torproject.org/onion-services/setup/
[advanced settings]: https://community.torproject.org/onion-services/advanced/
[tpo/onion-services/ecosystem#19]: https://gitlab.torproject.org/tpo/onion-services/ecosystem/-/issues/19

## General Advice

This section is adapted from the paper [Onion Services in the Wild: A Study of
Deanonymization Attacks] (pages. 309-310), which is released under a [Creative
Commons Attribution 4.0] license.

[Onion Services in the Wild: A Study of Deanonymization Attacks]: https://doi.org/10.56553/popets-2024-0117
[Creative Commons Attribution 4.0]: https://creativecommons.org/licenses/by/4.0/

### Essential advice for Tor beginners

While this documented is directed to service operators, we begin with general
recommendations for both Onion Service users and operators:

* These can help users stay safer when accessing Onion Services (and these
  advices will also help when surfing the web in general).
* Service operators can benefit from this basic advice as well:
    * By knowing what they can do if they plan to setup Onion Services but
      protecting their identities as service administrators themselves.
    * By having tips in what to recommend to their users in terms of best
      practices when interacting with Onion Services in a way that protect their
      privacy and anonymity.

These basic advices are summarized in the following table:

Advice                                                              | Investigative/intrusion method/attack vector if not implemented
--------------------------------------------------------------------|----------------------------------------------------------------
Create robust and unpredictable passphrases                         | Misconfigurations, linking information
Stay vigilant about metadata in files                               | Linking information
Use safe Tor browser settings                                       | Malware, misconfigurations
Choose privacy-focused providers in favorable jurisdictions         | Linking information, surveillance
Exercise caution with third-party interactions                      | Linking information, undercover infiltration
Implement end-to-end encryption                                     | Linking information, surveillance

#### Create robust and unpredictable passphrases

* Passphrases should not be predictable and sufficiently long.
* Avoid using easily discoverable personal information or common phrases.
* Remember, writing down your passphrase can pose a risk if discovered by
  others.

#### Stay vigilant about metadata in files

* Be mindful that files, including images and documents, can carry hidden data
  such as creation dates, locations, and device information.
* Cryptographic keys might contain sensitive information in comment fields or
  associated email addresses.
* Metadata can inadvertently reveal personal details.
* When sharing files, consider using tools or methods to strip or anonymize
  metadata to protect your privacy.

#### Use safe Tor browser settings

* Refrain from installing additional plugins in the Tor browser.
* Opt for the safest settings that deactivate JavaScript.
* While this may alter the appearance of websites, enabling JavaScript can
  introduce vulnerabilities that might compromise your anonymity.

#### Choose privacy-focused providers in favorable jurisdictions

* Opt for service providers known for their commitment to privacy.
* Ensure they operate within jurisdictions with transparent privacy laws and a
  strong stance on free speech.
* This approach can help reduce stored personal data.
* It also limits the likelihood and impact of data requests by authorities and
  breaches.

#### Exercise caution with third-party interactions

* Be aware that third parties may attempt to collect information or deceive you
  into revealing your real identity.
* Approach interactions with skepticism to safeguard your anonymity.

#### Implement end-to-end encryption

* Be aware of the fact that platforms, such as email services or forums, could
  be compromised, allowing unauthorized access to your messages.
* End-to-end encryption is crucial for protecting the content of your
  communications.
* End-to-end encryption does not conceal sender-receiver relations or other
  metadata.

### Advanced security practices for experienced Tor users

Like in the previous section, the following advice is useful both for Onion
Services users and operators.

Advice                                                              | Investigative/intrusion method/attack vector if not implemented
--------------------------------------------------------------------|----------------------------------------------------------------
Secure your devices with encryption                                 | Physical search
Maintain separate identities for different contexts                 | Linking information, crypto tracing
Understand that cryptocurrencies aren't inherently anonymous        | Crypto tracing
Handle downloaded files with caution                                | Malware, misconfigurations
Maintain a small online footprint                                   | Linking information
Use Tor bridges with pluggable transports to obfuscate Tor usage    | Surveillance, Tor protocol attacks (fingerprinting)
Diversify behaviors and characteristics across different identities | Linking information
Be mindful of your physical environment                             | Witnesses, surveillance

#### Secure your devices with encryption

* Utilize secure encryption software like [LUKS] to encrypt your file storage
  devices.
* Be aware that if you only encrypt certain files, systems may store temporary
  files in unencrypted locations.

[LUKS]: https://en.wikipedia.org/wiki/Linux_Unified_Key_Setup

#### Maintain separate identities for different contexts

* To avoid linking different contexts (like private social media and blogging),
  avoid reusing existing information.
* This includes pseudonyms and cryptographic keys.

#### Understand that cryptocurrencies aren't inherently anonymous

* Consider using privacy-focused cryptocurrencies like Monero.
* If you use pseudonymous cryptocurrencies like Bitcoin, which allow third
  parties to see the flow of funds, use decentralized mixing protocols like
  [CoinJoin] to obfuscate transaction flows.

[CoinJoin]: https://coinjoin.cash/

#### Handle downloaded files with caution

* As a general rule, avoid downloading files.
* If necessary, you can reduce the attack surface by opening downloaded files
  in a separate offline environment.
* Digital signatures can help to verify the integrity and authenticity of
  files.

#### Maintain a small online footprint

* Be mindful that attackers can use your public information to link to your
  identity.
* Remember that private information could also be leaked or hacked by
  attackers.

#### Use Tor bridges with pluggable transports to obfuscate Tor usage

* Tor bridges with pluggable transports can help to disguise Tor usage from
  your Internet Service Provider or third-parties in your local network.

#### Diversify behaviors and characteristics across different identities

* Avoid linking different user profiles together by varying login times,
  linguistic expressions, and other identifiable characteristics.
* It’s essential to consciously vary your behaviors and the details you share
  across different services or profiles.

#### Be mindful of your physical environment

* Stay aware of your physical surroundings and avoid behavior that might
  attract unwanted attention.

### Advice specific for Onion Service Operators

These are advices directed to Onion Service operators, intended to protect
_both_ the operators and the services:

Advice                                                                        | Investigative/intrusion method/attack vector if not implemented
------------------------------------------------------------------------------|----------------------------------------------------------------
Tunnel all traffic through Tor                                                | Misconfigurations
Implement secure log management                                               | Misconfigurations, physical search
Access your Onion Service exclusively through Tor                             | Surveillance, misconfigurations
Avoid reusing configurations and software code across multiple Onion Services | Shared characteristics, linking information
Limit information disclosure                                                  | Linking information, misconfigurations

#### Tunnel all traffic through Tor

* It's crucial to route all traffic through the Tor network to prevent
  accidental bypasses.
* Any traffic that doesn’t go through Tor could potentially expose the server’s
  real IP address or other identifying information.

#### Implement secure log management

* Log files can contain sensitive information that could potentially expose
  user identities or other confidential data.
* It's essential to manage these files securely and implement policies that
  limit the amount of sensitive information logged.

### Access your Onion Service exclusively through Tor

* Directly accessing your Onion Service without using Tor can link the service
  to your personal internet connection, potentially compromising your anonymity
  and the security of the service.

#### Avoid reusing configurations and software code across multiple Onion Services

* Shared characteristics, such as special configurations or reused software
  code, can potentially link several onion services together.
* This could compromise the anonymity of the services and their operators.

#### Limit information disclosure

* Configure software to minimize the amount of information it provides to
  clients.
* This includes disabling or altering services that reveal software versions
  and disabling unnecessary debugging information.
* Remove or anonymize identifiers.

## Recommendations for Onion Service Operators

This section contains general recommendations for setting up onionsites.
These are not strict requirements, but following them whenever makes sense will
significantly improve the user experience and safety.

### HTTPS support

[HTTPS for Onion Services][] is not a _strict_ requirement, but consider some
of the [benefits for having HTTPS][].

!!! info

    Depending on how your onionsite behaves in terms of HTTPS support,
    [the onion lock icon in Tor Browser may indicate different situations][].

Usually, there are three options involved when deciding whether to go for HTTP
or HTTPS:

1. Have a HTTP-only Onion Service:
    * Pros:
        * You won't need to deal with certificates :)
        * And you'll still get [Secure Contexts][] in Tor Browser.
    * Cons:
        * You might have issues with other content being loaded from HTTPS.
          That really depends on what a website is loading.
2. Have a certificate validated by a Certificate Authority (CA):
    * Pros:
        * This is the safer choice, with most guarantees to work.
        * You won't need to bother with mixed content issues etc.
        * Other [benefits for having HTTPS][] mentioned above.
    * Cons:
        * As of 2024-08, requires manual management (no ACME yet).
        * There's an annual cost for purchasing/renewing the certificate.
        * Reliance on the CA infrastructure.
        * Your certificate will probably be published on [CT Logs][], and
          hence your .onion address will be known publicly (which should
          be fine for public sites).
3. Have a self-signed certificate:
    * Pros:
        * Easy to setup and automate the renewal process.
        * Tor Browser currently won't display a warning page if you use
          self-signed certificates (as of 2024-08).
    * Cons:
        * Probably won't work on Brave Browser (as of 2023-05, it
           will display a warning page).
        * It may not be certain that Tor Browser will support this
          indefinitely without errors/warnings.
4. Use any other method described in the [certificate proposals][] page.

As of 2024-08, all are fine and up to you to choose which one fits best your
setup: there's currently no consensus on this, since each option have pros and
cons.

!!! note

    It's fine you start with Option 3 (self-signed certificate) and later move
    to Option 2 (CA-validated certificate).

    But it may be harder going later from HTTPS back to HTTP.

!!! tip

    If you do setup HTTPS, please make it the default (i.e, redirect HTTP
    requests to HTTPS).

!!! note

    You might also consider whether to get an [EV HTTPS certificate][ev-cert],
    but that can be very expensive and with a questionable benefit you can get
    from it in relation to a regular DV cert (the EV icon is disappearing from
    browsers lately).

[DV cert]: https://en.wikipedia.org/wiki/Domain-validated_certificate
[ev-cert]: https://en.wikipedia.org/wiki/Extended_Validation_Certificate

[HTTPS for Onion Services]: https://community.torproject.org/onion-services/advanced/https/
[benefits for having HTTPS]: ../../research/proposals/usability/certificates.md#benefits
[the onion lock icon in Tor Browser may indicate different situations]: https://support.torproject.org/onionservices/onionservices-5/
[Secure Contexts]: https://developer.mozilla.org/en-US/docs/Web/Security/Secure_Contexts
[CT Logs]: https://certificate.transparency.dev/
[certificate proposals]: ../../research/proposals/usability/certificates.md

### Service Discovery

Service discovery has to do with indicating to clients that a regular site has
an .onion counterpart.

#### Onion-Location

There are a [number of proposals for Service Discovery][], but as of December
2023 the recommended way is to deploy the [Onion-Location][] HTTP header or
tag.

When present, clients such as Tor Browser and Brave automatically parses this
header and shows the ".onion available" icon at the address bar, and may
automatically redirect to the onionsite depending on a config setting.

Please note that not all high-traffic sites use this header: some operators are
concerned that it increases the HTTP response size (an hence reduce
performance) for everyone in benefit of only a reduced number of Tor-enabled
users.

There's also a way to offer this header only for Tor users, by adding a rule in
your frontend proxies to check if requests are coming from a known public Tor
Exit node. This reduces HTTP response size for everyone except Tor users, at
the expense of some more server-side logic (and processing time). The
up-to-date list can be [fetched by a number of ways][] (and it changes very
often, so you'll need a scheduled task to keep your rule updated).

So in summary, there are basically three options for Onion-Location:

1. Setup [Onion-Location][] normally.

2. Setup in a way that your frontend proxies only present it when requests
   comes from the "Tor country" (i.e, from IPs in the public exit node list).

3. Do not set it up -- mostly due to performance concerns.

Recommendation is to start with option 1 (at least during the initial public
release phase) and then check whether you really have a performance issue
related to this small response size increase.

[number of proposals for Service Discovery]: ../../research/proposals/usability/discovery/README.md

#### Alt-Svc

Another approach -- and that may be no conflicting with Onion-Location, is to
setup [Alt-Svc][] headers pointing to the Onion Service, as [documented
here][]. As with Onion-Location, this header may also be set only for client
connections coming from the "Tor country".

Although the Alt-Svc approach is a very interesting (and transparent) way to
announce Onion Services, right now Tor Browser does not offers the same user
experience as with Onion-Location, such as indicating whether a given resource
was fetched through the Alt-Svc .onion address or through the regular resource
URL. So even if your users would be benefiting from Onion Service connections
announced via a Alt-Svc header, they would not be aware of that, and may even
believe the contrary, i.e, that their connection is happening through the
regular site.

Recommendation is to prioritize Onion-Location instead of Alt-Svc right now (as
of December 2023), due to the user experience difference.

[Onion-Location]: https://community.torproject.org/onion-services/advanced/onion-location/
[fetched by a number of ways]: https://blog.torproject.org/changes-tor-exit-list-service/
[Alt-Svc]: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Alt-Svc
[documented here]: https://blog.cloudflare.com/cloudflare-onion-service

### Load balancing/failover

For load balancing and redundancy, it's possible to run many tor daemon
instances in parallel to act as a Onion Service load balancing/failover layer.

Right now there are two approaches.

#### Parallel tor instances

This is the simpler approach, consisting in running multiple tor daemon
instances in parallel in different servers.

* Advantages:
    * Simpler to set up (just replicate what you already have).
    * Works well with the Proof Of Work anti-DoS defense ([PoW][]).
* Disadvantages:
    * Every tor server needs to have a copy of the .onion private key, so if
      one server is compromised then your service is compromised.
    * This is not "full" load balancing, acting mostly as a simple failover,
      and depends a lot in the timing you start each of the tor daemons.

[PoW]: ../../technology/pow.md

#### Using Onionbalance

[Onionbalance][] provides load-balancing and redundancy for Tor hidden services
by distributing requests to multiple backend Tor instances.

* Advantages:
    * Fully implements load balancing/failover.
    * Provides better isolation of the main .onion keys, reducing the attack
      surface.
* Disadvantages:
    * Harder to setup.
    * As of December 2023, it does not support the Proof Of Work anti-DoS defense ([PoW][]),
      but this is [being planned](https://gitlab.torproject.org/tpo/onion-services/onionbalance/-/issues/13).

[Onionbalance]: ../base/onionbalance/README.md

In short, you can have two kinds of servers/containers:

1. The publisher(s): only these will need access to the main .onion keys. No
   clients ever connect to this publishing node. It can be isolated from the
   other nodes as long as it can reach the Tor network.
2. The backend nodes, which will in fact serve connections:
    * If using HTTPS, backends will need to have the TLS private keys, which is
      least of a concern for Onion Services.
    * The backends will also use their own .onion keys, but these won't be used
      as addresses for connecting to the service.

This will only make sense if you can have a good isolation between backends and
the publisher nodes, otherwise this falls back to a regular load balancing
mechanism.

You'll have to make sure the backend containers don't have access to the main
.onion keys: you would need to have different file access schemes for each
container type.

As an example, [Onionspray] does have support for [Onionbalance] and [this
setup might work](onionspray/guides/balance/softmaps.md#complete-setup): the
configuration is slightly different: instead of "hardmaps", Onionspray uses
"softmaps" to configure Onionbalance.

Maybe this involve a lot of extra work, but you can migrate to this topology
anytime: after implementing the needed security fixes, you can keep your
existing configuration for quick recovery and move to [Onionbalance] later on.

#### Which one to go?

It depends. [Onionbalance][] usually is the best candidate, except if you plan to deploy
PoW.

If you prefer PoW over the other advantages offered by [Onionbalance][], I would
say use the simpler method instead.

You can also switch anytime from one approach to the other, without disrupting
the service.

As for the number of nodes, that will depend mostly on load/requests.

### Monitoring

The are two complimentary approaches to monitoring:

1. From the "outside" of your infrastructure, i.e, the monitoring tool can be
   set anywhere on the Internet, as long as the Tor network can be reached.
2. From the "inside", i.e, the monitoring tools are installed in the same
   local network (or server) where your Onion Services are running.

#### From the "outside"

The quickest way to monitor from the outside is to use [Onionprobe], maybe as a
[full monitoring node](onionprobe/standalone.md).

[Onionprobe]: onionprobe/README.md

#### From the "inside"

There are a few handy ways to monitor from the inside:

1. Analyzing tor daemon and the HTTP proxy logs using tools such as [Logstash][].
2. Using the `MetricsPort` option which exports data using Prometheus.

##### MetricsPort

This approach produces lots of useful data (maybe even more than you need) but
need to be set with care, so we quote here the [C tor][] daemon manual page:

    MetricsPort [address:]port [format]
        WARNING: Before enabling this, it is important to understand that
        exposing tor metrics publicly is dangerous to the Tor network users. Please
        take extra precaution and care when opening this port. Set a very strict access
        policy with MetricsPortPolicy and consider using your operating systems
        firewall features for defense in depth.

        We recommend, for the prometheus format, that the only address that
        can access this port should be the Prometheus server itself. Remember that the
        connection is unencrypted (HTTP) hence consider using a tool like stunnel to
        secure the link from this port to the server.

        If set, open this port to listen for an HTTP GET request to
        "/metrics". Upon a request, the collected metrics in the the tor instance are
        formatted for the given format and then sent back. If this is set,
        MetricsPortPolicy must be defined else every request will be rejected.

        Supported format is "prometheus" which is also the default if not
        set. The Prometheus data model can be found here:
        https://prometheus.io/docs/concepts/data_model/

        The tor metrics are constantly collected and they solely consists of
        counters. Thus, asking for those metrics is very lightweight on the tor
        process. (Default: None)

        As an example, here only 5.6.7.8 will be allowed to connect:

            MetricsPort 1.2.3.4:9035
            MetricsPortPolicy accept 5.6.7.8

    MetricsPortPolicy policy,policy,...
        Set an entrance policy for the MetricsPort, to limit who can access
        it. The policies have the same form as exit policies below, except that port
        specifiers are ignored. For multiple entries, this line can be used multiple
        times. It is a reject all by default policy. (Default: None)

        Please, keep in mind here that if the server collecting metrics on
        the MetricsPort is behind a NAT, then everything behind it can access it. This
        is similar for the case of allowing localhost, every users on the server will
        be able to access it. Again, strongly consider using a tool like stunnel to
        secure the link or to strengthen access control.

[Logstash]: https://www.elastic.co/logstash/
[Prometheus]: https://prometheus.io
[C Tor]: https://gitlab.torproject.org/tpo/core/tor

### Abuse protections

First, check the [Onion service DoS guidelines][] page for an overview and
initial steps to protect your service.

* If you want to use the [Proof of Work (PoW)](../../technology/pow.md) protection:
  * Start doing the stress test only after configuring PoW:
    * This will make sure your stress tests won't hurt the Tor network.

    * This will also test the service with the protections already in
      place.

  * One suggestion is to start with higher values for
    `HiddenServicePoWQueueRate` and `HiddenServicePoWQueueBurst` configuration
    params.

  * Then fine tune if according to your expected/actual load.

  * Load can be generated by whatever tool, as long as it's being
    proxied through Tor. Example: continuously spawning many instances
    of `torsocks wget http://your.onion`.

  * Try to load the onionsite with Tor Browser while your service is under the
    simulated load.

  * This is an example project showing how PoW limits DoS: [onion-pow-example][].

  * There are additional DoS mitigation options if you need, but PoW
    is a good start.

* Besides the previous recommendations, you can also control the number of
  introduction points (`HiddenServiceNumIntroductionPoints`) and check
  whether this improve performance and alleviate load.

[Onion service DoS guidelines]: https://community.torproject.org/onion-services/advanced/dos/
[onion-pow-example]: https://gitlab.torproject.org/tpo/onion-services/onion-pow-example

### Protecting your keys

* Keep an encrypted backup of the .onion keys.

* Also make sure to secure all servers hosting the .onion keys: if these keys
  are compromised, your Onion Service is compromised as well, forever, and you
  would need to generate a new set of keys/address. So general operational
  security applies here.

* You may also want to setup encrypted storage (and swap) in the servers,
  depending on your risk policy.

* Consider to never store the main .onion keys on any CI infrastruture. this
  might break automation, but the publisher node don't need to be rebuilt that
  often, and could be hosted in a separate infra.

* One thing that can be done is to keep encrypted/redundant backups of the
  .onion keys, and upload then in the services using tools like [Ansible], but
  note that having these keys in an automated provisioner node which can be a
  potential single point of failure.

[Ansible]: https://docs.ansible.com/ansible/

### Operational security

* Since it's an Onion Service, you may have very strict access policies to the
  servers running tor instances. Besides what you need in order to have console
  access, you can keep the server behind strict firewall policy for incoming
  connections. The tor daemon don't need to accept any incoming TCP connection
  from the outside (especially if you use the MetricsPort). It just need to
  have enough outbound internet access to connect to the Tor Network. In this
  sense, we usually say that an Onion Service server is actually an special
  kind of Tor client.

* If the tor process and the frontend HTTP proxy are in the same machine,
  consider using UNIX sockets to connect both instead of TCP (HiddenServicePort
  tor option), so you don't have local TCP port opened.

* [Riseup's Onion Services Best Practices][] guide is a bit outdated but still
  has some useful tips on securing your Onion Service.

[Riseup's Onion Services Best Practices]: https://help.riseup.net/en/security/network-security/tor/onionservices-best-practices

* Make sure the clocks are synchronized in all systems :)

### Performance

#### Single Hop Onion Service

!!! warning "Single Hop Mode discloses the service location"

    When using the feature described below, the Onion Service operator is choosing
    to improve performance at the cost of disclosing where the service is located.
    User location is not impacted by this feature.

For public-facing sites that aren't concerned in keeping the Onion Service
location secret, it is possible to reduce the number of hops to gain some
performance. Don't enable this if you want to keep the location of your
Onion Service secret.

!!! warning "Single Hop Mode is an instance-wide option"

    These settings are instance-wide, and not service-wide: once these
    settings are enable, _all_ Onion Services in the instance will be
    set to Single Hop Mode.

This operation mode is called "Single Hop Onion Services" and it's controlled
by two settings that need to be enabled. The `tor(1)` manual page portion
explaining how they work is quoted below:

    PER INSTANCE OPTIONS:

    HiddenServiceSingleHopMode 0|1
        Experimental - Non Anonymous Hidden Services on a tor
        instance in HiddenServiceSingleHopMode make one-hop (direct) circuits
        between the onion service server, and the introduction and rendezvous
        points. (Onion service descriptors are still posted using 3-hop paths,
        to avoid onion service directories blocking the service.) This option
        makes every hidden service instance hosted by a tor instance a Single
        Onion Service. One-hop circuits make Single Onion servers easily
        locatable, but clients remain location-anonymous. However, the fact that
        a client is accessing a Single Onion rather than a Hidden Service may be
        statistically distinguishable.

        WARNING: Once a hidden service directory has been used by a
        tor instance in HiddenServiceSingleHopMode, it can NEVER be used again
        for a hidden service.  It is best practice to create a new hidden
        service directory, key, and address for each new Single Onion Service
        and Hidden Service. It is not possible to run Single Onion Services and
        Hidden Services from the same tor instance: they should be run on
        different servers with different IP addresses.

        HiddenServiceSingleHopMode requires
        HiddenServiceNonAnonymousMode to be set to 1. Since a Single Onion
        service is non-anonymous, you can not configure a SOCKSPort on a tor
        instance that is running in HiddenServiceSingleHopMode. Can not be
        changed while tor is running. (Default: 0)

    HiddenServiceNonAnonymousMode 0|1
        Makes hidden services non-anonymous on this tor instance.
        Allows the non-anonymous HiddenServiceSingleHopMode. Enables direct
        connections in the server-side hidden service protocol. If you are using
        this option, you need to disable all client-side services on your Tor
        instance, including setting SOCKSPort to "0". Can not be changed while
        tor is running. (Default: 0)

!!! warning "Consider not to mix unrelated services in the Single Hop Mode"

    Consider **NOT** to use single mode/non-anonymous Onion Services
    (`HiddenServiceSingleHopMode` and `HiddenServiceNonAnonymousMode`) if
    distinct sites are hosted in the same provider/virtual machine and if
    relating each other is a concern. Suppose many distinct sites have their
    onions at the same place. Using single mode would mean it's easy to
    determine that these sites have their .onions hosted in the same location.
    By default `HiddenServiceSingleHopMode` and `HiddenServiceNonAnonymousMode`
    are not set, but depending on the tooling used to deploy this might not be
    the case.

In summary, these settings control whether _all_ Onion Services in a given tor
daemon instance should use the "Single Hop" mode. If they're enabled, users
will still connect to the Onion Service at a "rendezvous point" using the usual
three hops (which we could call as a "3 hop Tor circuit"), but the Onion
Service will connect to the rendezvous using only a single hop, hence "Single
Hop Onion Service".

### Towards offline main keys

For the future, an additional security measure might be available:
keeping .onion keys offline.

While this is [covered in the specs][offline-specs],

* This is not implemented on [C Tor], and probably [won't be][offline-c-tor].
* But there are plans to [implement this on Arti][offline-arti].

[offline-specs]: https://spec.torproject.org/rend-spec/protocol-overview.html?highlight=offline#imd-offline-keys
[offline-c-tor]: https://gitlab.torproject.org/tpo/core/tor/-/issues/29054
[offline-arti]: https://gitlab.torproject.org/tpo/core/arti/-/issues/1194

The "main" keys already generates the blinded keys which are used to sign the
descriptor. The missing piece seems to be a way to generate blinded keys in
advance and manage them.

Until this does not happen, [Onionbalance](#using-onionbalance) is what we have
to increase protection of the keys.

There are also plans to improve load balancing by [supporting it directly in
the main descriptor][improved-load-balancing], which could achieve security
gains similar to keeping main keys offline.

[improved-load-balancing]: https://gitlab.torproject.org/tpo/onion-services/onionbalance/-/issues/13#note_3010924

So far this has only been discussed here and there, and is not on any concrete
roadmap yet, as of 2025-01. Up-to-date status information about this and other
features is available in the [implementations
page](../../dev/implementations/README.md).

## Incident response

This section has a quick guidance in what to do in case of infrastructure attacks
that may potentially lead to unauthorized access on the Onion Service private keys.

So you suspect your Onion Service got compromised? Here's a quick checklist on
things you can do to try to identify whether an intrusion happened, and to
assess the damage.

### Investigating

The first step is investigating what may have happened.

Begin with standard computer forensics, which is mostly what needs to be done:
bring the system offline, preferentially in read-only mode, and proceed looking
for signs of infiltration.

Consider any potential attack vector, such as the backend application server in
use, like an web server and an web application framework.  As an example, if
you're using [Onionspray] for creating a onionsite proxy, [OpenResty]/[NGINX] is
used as the backend web server.

If your software is up-to-date, well configured and protected, potential
attacks would require a zero-day. Zero-days have huge market value, so they usually
aren't used in any situation. If your Onion Service is not considered a
"high-valued target", then probably a zero-day was not used. And if your service
had easier, well known vulnerabilities to exploit, why then burn a zero-day in
the first place?

Besides the standard procedures that you might find documented elsewhere, Onion
Services have some specificities that needs to be taken into account, like:

* If you find out that your setup was somehow compromised, it's maybe be safer
  to assume the Onion Service keys were compromised. The Tor daemon offers no
  protections against the superuser.

* If you know the timestamp when the Tor daemon last started, you can compare
  with the last time the .onion keys were read, but that could be prone to
  false positives if you have legitimate accessed these keys during your
  analysis. It's very easy to mess with reading times in file attributes.

* If you suspect that keys were stolen from the Tor daemon process
  itself, we might have a serious zero day here, like a "[Heartbleed]
  for Tor", so the full daemon logs could be helpful (and maybe memory
  dumps if you have). But before considering this worst case scenario,
  which might happen but it's unlikely, try first to focus on the general
  safety of your system and tools.

* If you have strong reasons to believe that there's a vulnerability in Tor
  software, you may want to put [security@torproject.org] in the loop (this
  e-mail address supports OpenPGP encryption), more info in [Tor's Security
  Policy page][tor-sec].

[Heartbleed]: https://en.wikipedia.org/wiki/Heartbleed
[tor-sec]: https://gitlab.torproject.org/tpo/core/team/-/wikis/NetworkTeam/SecurityPolicy
[Onionspray]: onionspray/README.md
[OpenResty]: https://openresty.org/
[NGINX]: http://nginx.org/
[security@torproject.org]: mailto:security@torproject.org

How can one be really sure that the Onion Service keys were stolen?

* One can only be sure when illegitimate use can be spotted.
* So suppose you stop using these keys, but still want to know if it was
  stolen. So just make [Onionprobe] or a simple script to monitor whether this
  service appears on Tor's Onion Service Directories (HSDirs).
* You can monitor the usage of the potentially stolen .onion keys with
  [Onionprobe], but that will just be effective if you decide to stop using
  these keys.
<!--
  The following needs confirmation.

  Non-anonymous mode outputs the following message:

    [warn] HiddenServiceNonAnonymousMode is set. Every hidden service on this tor
    instance is NON-ANONYMOUS. If the HiddenServiceNonAnonymousMode option is
    changed, Tor will refuse to launch hidden services from the same directories,
    to protect your anonymity against config errors. This setting is for
    experimental use only.

  But as of tor 0.4.7.16, it seems possible to switch back a service from
  single to multi-hop without even a warning. Needs further testing though.
-->
<!--
* If yours was a "Single Onion Service" (`HiddenServiceSingleHopMode`
  configuration on [C Tor]), then [C Tor] sets a special bit in the key
  file(s). If attackers are not careful enough to fix that, they'll only be
  able to start the service in the single mode, which could disclose the
  location of the rogue service.
-->

### What attackers can do if they get the service TLS key?

* If attackers only got the TLS private key, there's usually not much they can
  do to impersonate the live service.
* A potentially harmless thing they could do, if the TLS certificate allows,
  would be to sign statements and files.

### What attackers can do if they get the service private key?

If case attackers get the .onion key, they can:

* Impersonate the service.
* Try to keep the service offline, by uploading bogus descriptors: even if
  attackers don't spawn a full service, they can still DoS an existing service
  if they just upload fake descriptors.
* Generate a fresh TLS cert anytime. Nowadays this still require attackers to
  use some accepted payment method, but in the future they might get
  CA-validated certs for free, until the address is added in a block/exception
  list.

### Improving your security

Regardless of whether you found out whether the service keys were stolen, work
towards improving the security of your setup. This guide is a good starting
point.

### Generating new keys

If you want to be in the safe side regarding key leakage, the recommendation is
to:

1. Generate a new Onion Service key.
2. Get a new [HTTPS certificate](#https-support).

If you're using [Onion-Location], make sure to update it to the new
.onion address.

Also make sure to inform your users about this change, by means of
some "official" communication you might have, preferably using a
cryptographically signed message.

If you change the .onion keys and communicate well which is the correct address
(by using [Onion-Location] and also something like a blog post), then you may
be frustrating any sort of impersonation attacks relying on a potentially
stolen keypair.

### Restoring the service

Assuming you're already hardened you setup, consider additional measures
now to protect the .onion keys even more:

* [Setup Onionbalance](#using-onionbalance), so you can isolate a "publishing"
  node from the "backends".
* Use offline keys (if that's supported by your Tor daemon implementation).
