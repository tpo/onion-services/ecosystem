# Libraries

Onion Service libraries.

Last updated on 2024-11-20.

Library       | Description                                                                                                        | Interfacing        | Bindings for                                   | For [C Tor][]?         | For [Arti][]?
--------      | -------------                                                                                                      | -----------        | --------------                                 | --------               | ------
[Stem][]      | Python controller library for Tor with Onion Service support                                                       | [ControlPort][]    | [Python][]                                     | :white_check_mark: Yes | :x: No
[txtorcon][]  | An implementation of the [control-spec][ControlPort] for Tor using the Twisted networking library for [Python][]   | [ControlPort][]    | [Python][]                                     | :white_check_mark: Yes | :x: No
[Bine][]      | [Go][] library for accessing and embedding Tor clients and servers                                                 | [ControlPort][]    | [Go][]                                         | :white_check_mark: Yes | :x: No
[libCwtch][]     | Decentralized, privacy-preserving, multi-party messaging protocol for building metadata resistant applications     | [Bine][]           | [C][], [Go][], [Rust][]                        | :white_check_mark: Yes | :x: No
[Gosling][]   | For creating applications providing anonymous, secure, and private peer-to-peer functionality using Onion Services | [tor-interface][]  | [C][], [C++][], [Rust][], [Python][], [Java][] | :white_check_mark: Yes | Underway
[onyums][]    | [axum][] wrapper for serving web-based Onion Services                                                              | [tor-hsservice][]  | [Rust][]                                       | :x: No                 | :white_check_mark: Yes
[artiqwest][] | simple HTTP client that routes requests through the Tor network (can access Onion Services)                        | [arti-client][]    | [Rust][]                                       | :x: No                 | :white_check_mark: Yes
[torrosion][] | A Tor library for Rust                                                                                             | Own implementation | [Rust][]                                       | :x: No                 | :x: No

[Gosling]: http://gosling.technology/
[libCwtch]: https://git.openprivacy.ca/cwtch.im/autobindings/
[onyums]: https://crates.io/crates/onyums
[artiqwest]: https://crates.io/crates/artiqwest
[axum]: https://github.com/tokio-rs/axum
[Stem]: https://stem.torproject.org/tutorials/over_the_river.html
[txtorcon]: https://txtorcon.readthedocs.io/en/latest/guide.html#onion-hidden-services
[Bine]: https://github.com/cretz/bine
[torrosion]: https://crates.io/crates/torrosion

[Rust]: https://www.rust-lang.org/
[Python]: https://python.org
[Go]: https://go.dev/
[C]: https://en.wikipedia.org/wiki/C_(programming_language)
[C++]: https://en.wikipedia.org/wiki/C%2B%2B
[Java]: https://en.wikipedia.org/wiki/Java_(programming_language)

[Arti]: https://arti.torproject.org
[C Tor]: https://gitlab.torproject.org/tpo/core/tor
[ControlPort]: https://spec.torproject.org/control-spec/index.html
[tor-hsservice]: https://crates.io/crates/tor-hsservice
[tor-interface]: https://crates.io/crates/tor-interface
[arti-client]: https://crates.io/crates/arti_client

[cooked-apis]: https://gitlab.torproject.org/tpo/core/arti/-/issues/1357
