# Security Overview

In this page we document existing risk analysis and threat models about the
Onion Service technology itself or from other tools relying on it.

Last updated on 2025-02-06.

## Core Onion Services

* [Onion Service security overview at the vanguards documentation](https://github.com/mikeperry-tor/vanguards/blob/master/README_SECURITY.md).
* [Proving Security of Tor's Hidden Service Identity Blinding
  Protocol](https://www-users.cse.umn.edu/~hoppernj/basic-proof.pdf) (2013), offering
  proofs on descriptor unforgeability and unlinkability properties.
* [Proposal 344][] deals with protocol information leaks in Tor, and some of
  those may affect Onion Services.
* The research paper [Onion Services in the Wild: A Study of Deanonymization Attacks](https://petsymposium.org/popets/2024/popets-2024-0117.php)
  has a survey on existing de-anonymization techniques against Onion Services and it's users.
* Check also the [Research references](research/references/README.md)
  for further and deep discussion on Onion Services security.

## Tools

* [Onionbalance Security Overview](../apps/base/onionbalance/security.md).
* [Oniongroove Threat Model](https://tpo.pages.torproject.net/onion-services/oniongroove/threat/).
* [Onionspray security pages](../apps/web/onionspray/security/README.md).
* [Quiet Threat Model](https://github.com/TryQuiet/quiet/wiki/Threat-Model).
* [OnionShare Security Design (v2.6)](https://docs.onionshare.org/2.6/en/security.html).
* [Briar Threat Model](https://code.briarproject.org/briar/briar/-/wikis/threat-model).

[Proposal 344]: https://spec.torproject.org/proposals/344-protocol-info-leaks.html
