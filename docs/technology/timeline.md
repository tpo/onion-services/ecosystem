# Timeline

Last updated on 2024-09-19.

## About

This document is an incomplete summary of the main milestones in the history of
[Onion Services][].

For a historical overview, check [The Once and Future Onion][] article.

[Onion Services]: https://community.torproject.org/onion-services/
[The Once and Future Onion]: https://link.springer.com/chapter/10.1007/978-3-319-66402-6_3

## The technology

Year | Technology or relevant event
-----|---------------------------------------------------------------------------
1996 | [The Eternity Service][] and [Reply Onions][] designs
2003 | Hidden Service draft spec ([rendezvous][])
2004 | Hidden Service initial implementation on tor 0.0.6pre1 - 2004-04-08
2005 | New Hidden Service descriptor format on tor 0.1.1.2-alpha - 2005-07-15
2007 | Hidden Service v2 implementation on tor 0.2.0.7-alpha - 2007-09-21
2013 | [Proposal 224][] (Next Generation of Onion Services)
2014 | HTTPS certificates ([facebook][])
2014 | Vanity addresses   ([facebook][])
2015 | [CA/B Forum Ballot 144][]: validation rules for .onion names
2015 | [.onion addresses reserved by the IETF][] ([RFC 7686][])
2016 | Onion Services v3 initial development on tor 0.3.0.1-alpha - 2016-12-19
2016 | [Proposal 279][] (A Name System API for Tor Onion Services)
2017 | CA/B Forum Ballots [198][] and [201][]: .onion revisions
2017 | [Onion Services v3 spec][]
2018 | Onion Services v3 release on tor 0.3.2.9 - 2018-01-09
2018 | Alt-Svc ([cloudflare][])
2020 | Onion-Location ([TBB 9.5][])
2020 | Onion Authentication ([TBB 9.5][])
2020 | Onion Names for SecureDrop ([TBB 9.5][])
2020 | [CA/B Forum Ballot SC27v3][]: version 3 Onion Certificates
2020 | [#MoreOnionsPorfavor][]
2021 | Dreaming at Dusk: [NFT Auction][]
2021 | [Onion Services v2 final deprecation][] ([v2 deprecation timeline][])
2022 | [CA/B Forum Ballot SC54][]: Onion Cleanup
2022 | [DoS and the Onion Services Resource Coalition][]
2023 | [ACME for Onions][] Internet Draft
2023 | [Proposal 327][] (Proof-of-Work) [merged (#40634)][]
2023 | Onion Services 20th Anniversary (counting from the draft spec)
2024 | Onion Services 20th Anniversary (counting from the initial implementation)
2024 | [Initial Onion Services implementation in Arti][]

[The Eternity Service]: https://www.cl.cam.ac.uk/~rja14/eternity/eternity.html
[Reply Onions]: https://www.onion-router.net/Publications/IH-1996.pdf
[rendezvous]: https://gitlab.torproject.org/tpo/core/tor/-/commit/3d538f6d702937c23bec33b3bdd62ff9fba9d2a3
[facebook]: https://blog.torproject.org/facebook-hidden-services-and-https-certs/
[cloudflare]: https://blog.cloudflare.com/cloudflare-onion-service/
[CA/B Forum Ballot 144]: https://cabforum.org/2015/02/18/ballot-144-validation-rules-dot-onion-names/
[.onion addresses reserved by the IETF]: https://blog.torproject.org/landmark-hidden-services-onion-names-reserved-ietf/
[RFC 7686]: https://www.rfc-editor.org/info/rfc7686
[TBB 9.5]: https://www.torproject.org/releases/tor-browser-95/
[Proposal 224]: https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/proposals/224-rend-spec-ng.txt
[Proposal 279]: https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/proposals/279-naming-layer-api.txt
[198]: https://cabforum.org/2017/05/08/ballot-198-onion-revisions/
[201]: https://cabforum.org/2017/06/08/2427/
[Onion Services v3 spec]: https://spec.torproject.org/rend-spec-v3
[Onion Services v2 final deprecation]: https://support.torproject.org/onionservices/v2-deprecation/
[CA/B Forum Ballot SC27v3]: https://cabforum.org/2020/02/20/ballot-sc27v3-version-3-onion-certificates/
[#MoreOnionsPorfavor]: https://blog.torproject.org/more-onions-porfavor/
[NFT Auction]: https://blog.torproject.org/nft-auction-and-whats-next/
[v2 deprecation timeline]: https://blog.torproject.org/v2-deprecation-timeline/
[CA/B Forum Ballot SC54]: https://cabforum.org/2022/03/24/ballot-sc54-onion-cleanup/
[DoS and the Onion Services Resource Coalition]: https://blog.torproject.org/tor-network-ddos-attack/
[ACME for Onions]: https://acmeforonions.org
[Proposal 327]: https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/proposals/327-pow-over-intro.txt
[merged (#40634)]: https://gitlab.torproject.org/tpo/core/tor/-/issues/40634
[Initial Onion Services implementation in Arti]: https://blog.torproject.org/arti_1_1_12_released/

## Applications

Year | Application
-----|---------------------------------------------------------------------------
2010 | [GlobaLeaks][]
2010 | [Ricochet][]
2013 | [SecureDrop][]
2014 | [Onionshare][]
2015 | [Onionbalance][]
2017 | [Briar][]
2019 | [Wahay][]
2021 | [Ricochet-Refresh][] (first stable release with Onion Services v3 support)
2021 | [Gosling][]
2022 | [Quiet][]

[Onionshare]: https://support.torproject.org/onionservices/v2-deprecation/
[GlobaLeaks]: https://en.wikipedia.org/wiki/GlobaLeaks
[SecureDrop]: https://en.wikipedia.org/wiki/SecureDrop
[Onionbalance]: https://gitlab.torproject.org/tpo/core/onionbalance/-/commit/a8b970c7799a106440facd0ab9f0b19220a16e57
[Ricochet]: https://github.com/ricochet-im/ricochet/commit/8bf50c6d07bd61122828ed6870dadd1a52bd5794
[Ricochet-Refresh]: https://ricochetrefresh.net
[Gosling]: https://github.com/blueprint-freespeech/gosling/
[Wahay]: https://wahay.org
[Quiet]: https://tryquiet.org
[Briar]: https://briarproject.org
