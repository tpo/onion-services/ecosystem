# Contact

There are many ways to contact us about Onion Services!

## General Onion Services support

General support is done through the [Tor Forum][forum]:

* If you need technical support or have general questions about the technology,
  make a post in the [Onion Service category][category].

* For general updated about Onion Services, you might subscribe to the [Onion
  Service tag][tag].

[category]: https://forum.torproject.org/c/support/onion-services/16
[tag]: https://forum.torproject.org/tag/onion-services
[forum]: https://forum.torproject.org

## Reporting issues or suggesting updates in the documentation

Issues and suggestions related to this documentation can be made directly at
the [repository][]:

* If you want to report problems with this documentation, create an issue [here][issues].

* Submit merge requests to update this documentation [here][merges].

[repository]: https://gitlab.torproject.org/tpo/onion-services/ecosystem
[issues]: https://gitlab.torproject.org/tpo/onion-services/ecosystem/-/issues
[merges]: https://gitlab.torproject.org/tpo/onion-services/ecosystem/-/merge_requests

## Reporting issues or requesting changes in the code

Issue reports or merge requests can be opened directly on the corresponding
repository from the [Onion Services group][group] in the [Tor's GitLab
instance][gitlab].

[group]: https://gitlab.torproject.org/tpo/onion-services
[gitlab]: https://gitlab.torproject.org

## Other contact

Some Onion Service software might also have it's own, specific contact information.
Examples:

* [Onionbalance contact page](../apps/base/onionbalance/contact.md).
* [Onionspray contact page](../apps/web/onionspray/contact.md).
* [Oniongroove contact page](../apps/web/oniongroove/contact.md).
* [Onionprobe contact page](../apps/web/onionprobe/contact.md).
* [Onionmine contact page](../apps/web/onionmine/contact.md).
* [Onion Launchpad contact page](../apps/web/onion-launchpad/contact.md).
* [Research contact page](../research/about/contact.md).
